import React, { Component } from 'react';
import './Child.css';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import JoditEditor from "jodit-react";

const axios = require('axios');
// import { ReactVideo } from "reactjs-media";

class Child extends Component {
  // const [selectedFile, setSelectedFile];
  constructor(props){
    super(props);
    // [selectedFile, setSelectedFile] = useState(null);
    this.state = {
      medctrl: true,
      btnctrl: {
        imgcnt:true,
        vdcnt:false,
      },
      previewImage: undefined,
      previewVideo: undefined,
      setimgc: false,
      setvdo: false,
      fdata: {
       qvalue: '',
       picdata: null,
       viddata: null,
       answertype: '',
       answerlist: []
      },
      bactrl: 0
    }
    this.config = {readonly: false}
    this.addQuestionChange = this.addQuestionChange.bind(this);
    this.addAnswerChange = this.addAnswerChange.bind(this);
    this.selectFile = this.selectFile.bind(this);
  }
  jodit;
	setRef = jodit => this.jodit = jodit;
  
  onTrigger = (event) => {
    // event.preventDefault();
    // if(event.target.value !== undefined){
      // event.preventDefault();
      // let formData = new FormData()
      // formData.append('file', this.state.fdata.picdata)
      // formData.append('fname', 'praveen')
      // formData.append('lname', 'samal')
      // const config = {     
      //   headers: { 'content-type': 'multipart/form-data' }
      // }
      // axios.post("http://localhost:3001/upload", formData,config, {}).then(res => {
      //   console.log(res.statusText)
      // })
      var fdata = this.state.fdata;
      this.setState({
        medctrl: true,
        btnctrl: {
          imgcnt:true,
          vdcnt:false,
        },
        previewImage: undefined,
        previewVideo: undefined,
        setimgc: false,
        setvdo: false,
        fdata: {
         qvalue: '',
         picdata: null,
         viddata: null,
         answertype: '',
         answerlist: []
        },
        bactrl: 0
      });
      event.preventDefault();
      this.props.parentCallback(fdata);

  }
  addQuestionChange = (quevalue) => {
    var someProperty = {...this.state.fdata}
    someProperty.qvalue = quevalue;
    this.setState({fdata: someProperty})
  }

  onbtnTrigger = (event) => {
    let ctrdata = event.target.value;
    var someProperty = {...this.state.btnctrl}
    if(ctrdata === 'Add Image'){
      someProperty.imgcnt = false;
      someProperty.vdcnt = true;
      this.setState({
        medctrl: false,
        btnctrl: someProperty
      })
    } else {
      someProperty.imgcnt = true;
      someProperty.vdcnt = false;
      this.setState({
        medctrl: true,
        btnctrl: someProperty
      })
    }
  }

  selectFile = (file,accept) => {
    let ctrdata = accept;
    var someProperty = {...this.state.fdata}
    someProperty.picdata = file;
    if(ctrdata === 'image/*'){
      this.setState({
        previewImage: URL.createObjectURL(file),
        setimgc: true,
        fdata: someProperty
      })
    } else{
      this.setState({
        previewVideo: URL.createObjectURL(file),
        setvdo: true,
        fdata: someProperty
      })
    }
  }

  selectAnswer = (event) => {
    var someProperty = {...this.state.fdata}
    someProperty.answerlist = []
    if(event.target.value ==='Single Choice'){
      someProperty.answertype = 'singlehoice'
      this.setState({
        bactrl: 1,
        fdata: someProperty
      })
    }
    else if(event.target.value ==='Multiple Choice'){
      someProperty.answertype = 'multichoice'
      this.setState({
        bactrl: 2,
        fdata: someProperty
      })
    }else{
      someProperty.answertype = 'textbox'
      this.setState({
        bactrl: 3,
        fdata: someProperty
      })
    }
  }
  
  addAnswerChange = (answerdata,key)=>{
    var someProperty = {...this.state.fdata}
    someProperty.answerlist[key] = answerdata
    this.setState({
      fdata: someProperty
    })
  }
  
  addAnswer = (event) =>{
    var someProperty = {...this.state.fdata}
    someProperty.answerlist.push('');
    this.setState({
      fdata: someProperty
    })
  }
  render() {
    return(
      <div>
          <form onSubmit = {this.onTrigger}>
              Enter Question: 
              {/* <input type = "text" name = "fquestion" placeholder = "Enter Question" onChange={e => this.addQuestionChange(e.target.value)}/> */}
              <JoditEditor
                // ref={this.editor}
                editorRef={this.setRef}
                value={this.state.fdata.qvalue}
                config={this.config}
		            // onBlur={newContent => this.setState({content: newContent})}
                onChange={newContent => this.addQuestionChange(newContent)}
              />
              <br></br>
              Enter Question Type:
              <input value="Add Image" onClick={this.onbtnTrigger} type="button" disabled={this.state.btnctrl.imgcnt}/>{' '}
              <input value="Add Video" onClick={this.onbtnTrigger} type="button" disabled={this.state.btnctrl.vdcnt} />

              {this.state.medctrl?
              <div>
                Upload Image <input type="file" accept="image/*" onChange={e => this.selectFile(e.target.files[0],e.target.accept)} />
                {this.state.setimgc?
                  <img className="preview" id="imgId" src={this.state.previewImage} alt="" height="500px" width="500px"/> :
                  <> </>
                }
              </div>
              :
              <div>
                Upload Video <input type="file" accept="video/*" onChange={e => this.selectFile(e.target.files[0],e.target.accept)} />
                {this.state.setvdo?
                  <video width="50%" height="250" controls>
                    <source id="vdoId" src={this.state.previewVideo} type="video/" />
                  </video>
                :<></>
                }
              </div>}
              <br></br>
              Enter Answer type: <input type="button" value="Single Choice" onClick={this.selectAnswer} />{' '}
              <input type="button" value="Multiple Choice" onClick={this.selectAnswer} />{' '}
              <input type="button" value="True False" onClick={this.selectAnswer} />
              <br></br>
              {(this.state.bactrl === 1 || this.state.bactrl === 2)?
               <div>
                 <input type="button" value="addanswer" onClick={this.addAnswer} text="Add Answers" />
                 {this.state.fdata.answerlist.map((mdata,index)=>{
                   return (
                     <div key={index.toString()}>
                       <input 
                        type="text" 
                        value={mdata} 
                        placeholder="Enter Answer" 
                        onChange={e => this.addAnswerChange(e.target.value,parseInt(index.toString()))}/>
                     </div>
                   )
                 })}
               </div>:<></>
              }
              <input type="submit" value="Submit"/>
              <br></br><br></br>
          </form>
      </div>
      )
  }
}
export default Child;